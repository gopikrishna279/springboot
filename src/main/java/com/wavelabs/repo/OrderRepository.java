package com.wavelabs.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wavelabs.api.request.Order;

public interface OrderRepository extends JpaRepository<Order, Integer> {

	public List<Order> findByCustomer_customerId(int id);
	
	//public List<Order> findAllOrderByCustomer_customerName();

	//public List<Order> findAllByOrderByCustomer_customerName();

}

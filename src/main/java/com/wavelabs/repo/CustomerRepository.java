package com.wavelabs.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wavelabs.api.request.Customer;

public interface CustomerRepository extends JpaRepository<Customer,Integer> {

}

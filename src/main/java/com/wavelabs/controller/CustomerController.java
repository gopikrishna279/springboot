package com.wavelabs.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.api.request.Customer;
import com.wavelabs.api.request.Order;
import com.wavelabs.exception.CustomerNotFoundException;
import com.wavelabs.repo.CustomerRepository;
import com.wavelabs.repo.OrderRepository;

@RestController
public class CustomerController {

	@Autowired
	private CustomerRepository customerRepo;

	@Autowired
	private OrderRepository orderRepo;

	@PostMapping("/customers")
	public ResponseEntity createCustomer(@RequestBody Customer customer) {
		try {
			customer = customerRepo.save(customer);
			return ResponseEntity.status(201).body(customer);
		} catch (Exception e) {

			return ResponseEntity.status(500).body("Something wnet wrong");
		}

	}

	@PostMapping("/customers/{id}/orders")
	public ResponseEntity postOrder(@PathVariable("id") int customerId, @RequestBody Order order)  {
		Optional<Customer> customer = customerRepo.findById(customerId);
		if (customer.isPresent()) {
			order.setCustomer(customer.get());
		} else {
			throw new CustomerNotFoundException("Invalid Customer");
		}
		try {
			order = orderRepo.save(order);
			return ResponseEntity.status(201).body(order);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(500).body("Something wnet wrong");
		}
	}

	@GetMapping("/customers/{id}/orders")
	public ResponseEntity getCustomerOrders(@PathVariable("id") int customerId) {
		List<Order> orders = orderRepo.findByCustomer_customerId(customerId);

		return ResponseEntity.status(200).body(orders);
	}

	@GetMapping("/customers/orders")
	public ResponseEntity getAllOrders() {
		List<Order> orders = orderRepo.findAll(Sort.by(Sort.Direction.ASC, "customer_customerName"));
		return ResponseEntity.status(200).body(orders);
	}

	@GetMapping("/words")
	public ResponseEntity findNoOfWords(@RequestParam("text") String text) {
		Map<String, Integer> map = new HashMap();
		String[] words = text.split(" ");
		for (String word : words) {
			Integer count = map.get(word);
			if (count == null) {
				map.put(word, 1);
			} else {
				map.put(word, count + 1);
			}
		}
		return ResponseEntity.status(200).body(map);
	}

}

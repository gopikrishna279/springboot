package com.wavelabs.exception;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class HanldeExceptions {

	@ExceptionHandler(CustomerNotFoundException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseEntity handleInputValidationException(CustomerNotFoundException e, HttpServletRequest request) {
		Map<String, String> map = new HashMap();
		map.put("message", "customer not found with given id");
		return ResponseEntity.status(400).body(map);
	}
}

FROM dockreg.use.dom.carezen.net/dockerhub/anapsix-alpine-java:8

ADD target/Ebsynergies-1.jar app.jar
#setting heap memory to 4GB
CMD [ "java","-Xmx4096m", "-jar", "/app.jar" ]